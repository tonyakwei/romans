
package romannumerals;

import java.util.HashMap;
import java.util.Scanner;

/**
* Roman Numeral <--> Decimal Converter.
* 
* <P>Class converts Roman numerals to decimals.

* @author Anthony Addo
* @version 0.3
*/

public class RomanNumerals {

    
    static HashMap<Character, Integer> numerals = new HashMap(); //look-up table for roman numerals
    
    //standard roman numerals added to look-up table
    public static void init(){
        numerals.put('I', 1);
        numerals.put('V', 5);
        numerals.put('X', 10);
        numerals.put('L', 50);
        numerals.put('C', 100);
        numerals.put('D', 500);
        numerals.put('M', 1000);
        
    }
    
    public static void main(String[] args) {
        init(); //sets roman numeral valuesX
        
        //continually takes user input
        System.out.println("Enter some Roman Numerals or Decimal Integers!");
        Scanner sysInput = new Scanner(System.in);
        String line;
        while(!(line = sysInput.nextLine()).isEmpty()){
            if(line.matches("^-?\\d+$")){ //***some boiler-plate regular expression for digit asserting I took from a previous project
               System.out.println("--->    " + i2r(Integer.parseInt(line))); 
            }
            else System.out.println("--->    " + r2i(line));
        }
        

        
    }
    
       /**
  * Calculate decimal value of a given Roman Numeral
  * 
  * @param romanNumerals the Roman Numerals whose decimal value is to be calculated
  * @return the decimal representation of the passed Roman Numerals.
  * 
  * @exception NullPointerException if string contains non Roman Numeral character, or is empty or null
  * @exception NumberFormatException if the Roman Numerals are improperly formatted
  * @exception IllegalStateException if the string is empty
  */
    
    public static int r2i(String romanNumerals){
        int sum = 0; //keeps track of calculated binary
            
        try{ //as many errors can be thrown
            
            //boundary cases
            if(romanNumerals == null || romanNumerals.isEmpty()) throw new IllegalStateException();
            
            for(int c = 0; c < romanNumerals.length(); c++){
                char currentChar = romanNumerals.charAt(c);
                
                //if current character is the first character or no subtraction needs to be performed, do regular addition
                if(c == 0 || !(pv(currentChar) > pv(romanNumerals.charAt(c - 1)))){ //exploitng Java's short-circuiting mechanic
                    int addend = numerals.get(currentChar);
                    sum += addend;
                    continue;
                }
                
                //place values of current Roman Numeral and previous Roman Numeral
                int currentPV = pv(currentChar);
                int prevPV = pv(romanNumerals.charAt(c - 1));
                
                //either subtraction will need to occur, or the Roman Numerals are formatted incorrectly
                if(currentPV > prevPV){
                    if(currentPV > prevPV + 1) throw new NumberFormatException(); //e.g. IC - this is an improper Roman Numeral
                    if(c >= 2){
                        if(prevPV >= pv(romanNumerals.charAt(c - 2))) throw new NumberFormatException(); //e.g. IIV - this is an improper Roman Numeral
                    }
                    
                    //if errors are not thrown, regular subtraction occurs
                    sum -= (2 * numerals.get(romanNumerals.charAt(c - 1)));
                    sum += (numerals.get(currentChar));
                }

            }
        }
        
        //error catching
        catch(NullPointerException e){
            System.err.println("String contains non Roman numeral character"); System.exit(1);
        }
        catch(NumberFormatException e){
            System.err.println("Roman numeral is not formatted correctly"); System.exit(2);
        }
        catch(IllegalStateException e){
            System.err.println("Roman numeral string is empty"); System.exit(3);
        }
        catch(Exception e){
            System.err.println("The Roman numeral has a problem"); System.exit(4);
        }
        
        return sum;
}
    
    //Overloading place value method
    
    /**
  * Calculate place value of a given Roman Numeral
  * 
  * @param romanNumeral the Roman Numeral whose place value is to be calculated
  * @return the place value in terms of exponent of 10. e,g, C = 100 returns 2.
  */
    public static int pv(char romanNumeral){
        int number = numerals.get(romanNumeral);
        return pv(number);
    }
    
     /**
  * Calculate place value of a given number
  * 
  * @param number the the number whose place value is to be calculated
  * @return the place value in terms of exponent of 10. e,g, C = 100 returns 2.
  */
    
    public static int pv(int number){
        int pv = 0;
        while(number > 9){
            number /= 10;
            pv++;
        }
        return pv;
    }
    
          /**
  * Calculate Roman Numeral string of a given decimal integer
  * 
  * @param number the decimal integer whose Roman Numeral value is to be calculated
  * @return the Roman Numeral representation of the passed decimal integer
  * 
  * @exception NumberFormatException if number is either above 3999 or nonpositive.

  */
    
    public static String i2r(int number){
        StringBuilder romanNumerals = new StringBuilder(); //for holding string of Roman Numerals 
        try{
            if(number > 3999 || number < 1) throw new NumberFormatException(); //given bounds
            int currentPV = pv(number); //gets maximum place value of given number and holds it
            
            while(currentPV >= 0){
                if(currentPV == 3){
                    int thousands = number / 1000 % 10;
                    for(int t = 0; t < thousands; t++) romanNumerals.append("M");
                    currentPV--;
                }
            
                if(currentPV == 2){
                    int hundreds = number / 100 % 10; //gets value of hundreds place
                    //placevalue -= placevalue sets the placevalue to 0, ensuring that no extra values will be added in the for loop
                    if(hundreds == 4){ romanNumerals.append("CD"); hundreds -= hundreds; }
                    else if(hundreds >= 5 && hundreds != 9){ romanNumerals.append("D"); hundreds -= 5; }
                    else if(hundreds == 9){ romanNumerals.append("CM"); hundreds -= hundreds; }
                    for(int h = 0; h < hundreds; h++) romanNumerals.append("C");
                    currentPV--;
                }

                if(currentPV == 1){
                    int tens = number / 10 % 10;
                    if(tens == 4){ romanNumerals.append("XL"); tens -= tens;}
                    else if(tens >= 5 && tens != 9){ romanNumerals.append("L"); tens -= 5; }
                    else if(tens == 9){ romanNumerals.append("XC"); tens -= tens; }
                    for(int t = 0; t < tens; t++) romanNumerals.append("X");
                    currentPV--;
                }

                if(currentPV == 0){
                    int ones = (number % 10); //simply for repetitive clarity
                    if(ones == 4){ romanNumerals.append("IV"); ones -= ones; }
                    else if(ones >= 5 && ones != 9){ romanNumerals.append("V"); ones -= 5; }
                    else if(ones == 9){ romanNumerals.append("IX"); ones -= ones; }
                    for(int o = 0; o < ones; o++) romanNumerals.append("I");
                    return romanNumerals.toString();
                }
            }
            
            
        }catch(NumberFormatException e){
            System.err.println("Given number is either too high or nonpositive."); System.exit(1);
        }
        
        return "An error occured.";
    }
    
    
}
