# Roman Numerals <--> Decimals
## By Anthony Addo, for Pega.

This program can take given Roman Numerals or Decimals and given the alternative representation. It takes input via stdin until an error occurs.
No more confusion with MMMXC or XVII!

A LaTeX report can also be found here: https://drive.google.com/file/d/0Bw1NCKDkcxisaUF5Sldqa1RlUDQ/view?usp=sharing